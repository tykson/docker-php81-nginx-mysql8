**DOCKERIZE PHP**
==============================

Docker containers for php8.1-FPM,NginX,MySQL 8.0.

*Remember:*
- Make sure you have docker installed
@url: https://docs.docker.com/engine/install/ubuntu/*

- Dont run docker as a root, instead add docker group to your current user.
@url:https://www.configserverfirewall.com/ubuntu-linux/add-user-to-docker-group-ubuntu/

- Install docker compose.
@url: https://docs.docker.com/compose/install/

**Containers**
==============================
- Php-8.1-FPM
- MySQL 8.0
- Nginx (latest version)



**To run**
==============================
$ make build

$ make run

$ make ssh-docker (ssh to backend)

**Services**
==============================
- Webserver http://localhost:800
- PHP-FPM: 9000
- MySQL: 3310
- Composer
- Xdebug (linux only)